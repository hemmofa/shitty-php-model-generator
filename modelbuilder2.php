<?php 
echo "<pre>";

$modelname = $_POST["name"];
$inputvars = $_POST["inputvars"];
$outputvars = $_POST["outputvars"];
$vars = explode(",", $outputvars);
$invars = explode(",", $inputvars);

// Start of class:

echo "class " . $modelname . "{\n";

// Generate Private Vars
foreach ($vars as $var) {
	echo "\tprivate $" . $var . ";\n";
}
//echo "\n";

// Generate  Construct Function
echo "\tpublic function __construct(";

foreach ($vars as $var) {
	echo "$" . $var . ", ";
}

echo ") { // TODO: Remove comma after last var (and this comment)\n";

foreach ($vars as $var) {
	echo "\t\t" . '$this->' . $var . " = $" . $var . ";\n";
}
echo "\t}\n\n";


// Generate Getters
foreach ($vars as $var) {
echo "\tpublic function get" . ucfirst(strtolower($var)) . "(){\n";
echo "\t\treturn " . '$this->' . $var . ";\n";
echo "\t}\n";
}
echo "}\n\n";

// End of class

// Start of Model

echo "class model {\n";

// Generate Private Vars
foreach ($invars as $var) {
	echo "\tprivate $" . $var . ";\n";
}
//echo "\n";

// Generate  Construct Function
echo "\tpublic function __construct(";

foreach ($invars as $var) {
	echo "$" . $var . ", ";
}

echo ") { // TODO: Remove comma after last var (and this comment)\n";

foreach ($invars as $var) {
	echo "\t\t" . '$this->' . $var . " = $" . $var . ";\n";
}
echo "\t}\n\n";

// Generate Model datagetter
echo "\tpublic function get" . ucfirst(strtolower($modelname)) . "s(";

foreach ($invars as $var) {
echo "$" . $var . ", ";
}
echo ") { // TODO: Remove comma after last var (and this comment)\n";

echo "\n";
echo "\t\t\t" . '$database = new Database();' . "\n";
echo "\t\t\t" . "if($" . $invars[0] . " == null) {\n";

echo "\t\t\t\t" . 'echo $database->query();' . " // TODO: Put Query without WHERE clause here\n";
echo "\t\t\t} else {\n";
echo "\t\t\t\t" . 'echo $database->query();' . " // TODO: Put Query with WHERE clause here\n";
echo "\t\t\t\t // TODO: Put binds here. Example: " . '$database->bind(\':userid\', $userid);' . "\n";
echo "\t\t\t}\n";
echo "\t\t\t" . '$' . $modelname . 's = $database->resultset();' . "\n";

echo "\t\t\t\$return" . $modelname . 's = array();' . "\n";
echo "\t\t\tforeach( $" . $modelname . "s as $" . $modelname . ") {\n";
echo "\t\t\t\t// TODO: Do additional stuff with data if neccesary\n";
echo "\t\t\t\tarray_push(\$return" . $modelname . "s, new " . $modelname . "(";

foreach ($vars as $var) {
//echo "$" . $var . ", ";
echo "$" . $modelname . "['" . $var . "'], ";

}
echo "));" . " // TODO: Put Query with WHERE clause here\n";
echo "\t\t\t}\n";
echo "\t\treturn \$return" . $modelname . "s;\n";
echo "\t}\n";
echo "}\n";
echo "</pre>";
